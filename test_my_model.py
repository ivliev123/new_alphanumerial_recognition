import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import time
mnist  = tf.keras.datasets.mnist
import cv2

array_for_bace = ["B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z",  "A", "E", "I", "O", "U", "Y", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]


img_path = '/home/ivliev/alphanumeric_recognition/base/7.1.jpg'

img = cv2.imread(img_path,0)
img = cv2.resize(img,(int(28),int(28)))
ret,img = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
print(img)
img = img/255.0
print(img)

data = img

test_array = []
test_array.append(data)
test_array = np.array(test_array)

new_model = tf.keras.models.load_model('alphanumer.model')

x1 = time.time()
predictions = new_model.predict(test_array)
x2 = time.time()
print(x2-x1)
# print (len(x_test))
# print( x_test.shape )
# print( test_array.shape )



print(np.argmax(predictions[0]))
print(array_for_bace[np.argmax(predictions[0])])


plt.imshow(data,cmap=plt.cm.binary)
plt.show()
