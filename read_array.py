import pickle
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt


with open('data.pickle', 'rb') as f:
    data_new = pickle.load(f)

array_for_bace = ["B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z",  "A", "E", "I", "O", "U", "Y", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

x_train = []
y_train = []

# print(data_new)
data_new = np.array(data_new)
print(data_new.shape[0])


for i in  range(data_new.shape[0]):
    x_train.append(data_new[i][0][0])
    y_train.append(array_for_bace.index(data_new[i][1]))

print(len(y_train))
print(len(x_train))

y_train = np.array(y_train)

print(type(y_train[0]))
y_train.astype(np.uint8)

print(type(y_train[0]))

x_train = tf.keras.utils.normalize(x_train, axis = 1)

model =  tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(512,activation = tf.nn.relu))
model.add(tf.keras.layers.Dense(512,activation = tf.nn.relu))
model.add(tf.keras.layers.Dense(512,activation = tf.nn.relu))
model.add(tf.keras.layers.Dense(len(array_for_bace),activation = tf.nn.softmax))


model.compile(optimizer = 'adam',
                loss = 'sparse_categorical_crossentropy',
                metrics=['accuracy'])

model.fit(x_train, y_train, epochs=10)



model.save('alphanumer.model')



#
# for i in range(3):
#     print(x_train[i])
#     print()
