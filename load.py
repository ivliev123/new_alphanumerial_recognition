import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import time
mnist  = tf.keras.datasets.mnist


(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train = tf.keras.utils.normalize(x_train, axis = 1)
x_test = tf.keras.utils.normalize(x_test, axis = 1)


data = x_test[100]

test_array = []
test_array.append(data)
test_array = np.array(test_array)

new_model = tf.keras.models.load_model('epic_num_reader.model')

x1 = time.time()
predictions = new_model.predict(test_array)
x2 = time.time()
print(x2-x1)
# print (len(x_test))
# print( x_test.shape )
# print( test_array.shape )


print(predictions)
print(np.argmax(predictions[0]))

print(len(y_train))
# plt.imshow(data,cmap=plt.cm.binary)
# plt.show()
