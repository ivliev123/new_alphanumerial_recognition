import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont
import glob, os
import copy
import imutils






newpath = "base_cut"
if not os.path.exists(newpath):
    os.makedirs(newpath)


for root, dirs, files in os.walk("/home/ivliev/tensorflow/base"):
    print(root[-1])
    newpath = "/home/ivliev/tensorflow/base_cut/" + root[-1]
    if not os.path.exists(newpath):
        os.makedirs(newpath)

    os.chdir(root)
    for file in glob.glob("*.png"):
        image_name = root+'/'+file
        # print(image_name)
        img = cv2.imread(image_name,0)
        ret,thresh = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
        # cv2.imwrite(newpath +'/'+ file,thresh)
        contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,
        	cv2.CHAIN_APPROX_SIMPLE)
        print (len(contours))
        if len(contours)==1:
            contours = contours[1] if imutils.is_cv3() else contours[0]
            # print(contours)
            x,y,w,h = cv2.boundingRect(contours)
            cut_img = img[y:y+h, x:x+w]
            cut_img = cv2.resize(cut_img,(int(28),int(28)))

            cv2.imwrite(newpath +'/'+ file,cut_img)

        # font = ImageFont.truetype(font_tt, size=60)
        # image = Image.open('/home/ivliev/tensorflow/white_image.png')
        # draw = ImageDraw.Draw(image)
        # draw.text((x, y), latter, fill=color, font=font)
        # k +=1
        # image.save('/home/ivliev/tensorflow/base/'+latter+'/'+file[0:-4]+'_'+latter+'.png')
